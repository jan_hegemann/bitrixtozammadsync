import sys
import requests
import json
import time
import types
import os




BITRIX_URL = 'https://b24-id4qme.bitrix24.de/rest/1'
BITRIX_SECRET = os.environ['eins']
ZAMMAD_URL = 'http://'+os.environ['zwei']+'/api/v1/users/'
ZAMMAD_URLC = 'http://'+os.environ['zwei']+'/api/v1/organizations/'
ZAMMAD_TOKEN = os.environ['drei']

def loggen(pText):
    print(time.strftime("%d.%m.%Y %H:%M:%S")+':'+pText)

def get_customers(url, secret) -> list:
    params = {
        'select[NAME]':'NAME',
        'select[LAST_NAME]':'LAST_NAME',
        'select[EMAIL_VALUE]': 'EMAIL',
        'select[PHONE]': 'PHONE',     
        'select[COMPANY_ID]':'COMPANY_ID',
        
    }
    return get(url, secret, 'crm.contact.list', params)
    
def get_customersev(url, secret) -> list:
    params = {
        
        'select[NAME]':'NAME',
        'select[LAST_NAME]':'LAST_NAME',
        'select[EMAIL_VALUE]': 'EMAIL',
        'select[PHONE]': 'PHONE',     
        'select[COMPANY_ID]':'COMPANY_ID',
    }
    return get(url, secret, 'crm.contact.list', params)    

def get_companies(url, secret) -> list:
    params = {
        'select[TITLE]': 'TITLE',
        
        
    }
    return get(url, secret, 'crm.company.list', params)


def get(url, secret, method, params={'start': 0}) -> list:
    if not 'start' in params:
        params['start'] = 0
    results = []
    while True:
        r = requests.get(f'{url}/{secret}/{method}', params=params)
        r.raise_for_status()
        d = json.loads(r.text)

        if type(d['result']) == list:
            results.extend(d['result'])
        else:
            results.append(d['result'])

        if 'next' in d:
            params['start'] = d['next']
        else:
            return results


def set(url, secret, method, data=None):
    r = requests.post(f'{url}/{secret}/{method}', data=data)
    return r.json().get('result')
    
def update(url, secret, method, data=None):
    r = requests.put(f'{url}/{secret}/{method}', data=data)
    return r

def syncBITRIXToZAMMAD():
    loggen("Synchronisation gestartet mit Parametern:"+BITRIX_URL+' '+BITRIX_SECRET+' '+ ZAMMAD_URL+' '+ ZAMMAD_URLC+' '+ ZAMMAD_TOKEN)
    loggen("Unternehmen werden Synchronisiert:")
    out = get_companies(BITRIX_URL, BITRIX_SECRET)
    l = [ ]
    x = 0
    for item in out:
        ll = []
        for key, value in out[x].items():
            if (isinstance(value, list)):
                ll.append((key, value[0]["VALUE"]))
            else:
                ll.append((key, value))
        id = (out[x]['ID'])
        title = (out[x]['TITLE'])    
        setZammadC(title, id)
        updateZammadC(title, id)
        l.append(ll) 
        x = x+1    
    loggen(str(x)+" Firmen wurden bearbeitet")    
    loggen("Kunden werden Synchronisiert:") 
    
    out = get_customers(BITRIX_URL, BITRIX_SECRET)
    l = [ ]
    x = 0
    for item in out:         
        ll = []
        for key, value in out[x].items():
            if (isinstance(value, list)):
                ll.append((value[0]["VALUE"]))
            else:
                ll.append((value))
                
        id = (out[x]['ID'])
        name = (out[x]['NAME'])
        lastname = (out[x]['LAST_NAME'])
        email = (out[x]['EMAIL'][0]["VALUE"])
        tel = (out[x]['PHONE'][0]["VALUE"])
        comp = (out[x]['COMPANY_ID'])
        
        setZammad(name, lastname, id, email, tel, comp)
        updateZammad(name, lastname, id, email, tel, comp)
        l.append(ll)  
        x = x+1
    loggen(str(x)+" Kunden wurden bearbeitet")    
   
    print("-----------------------------------------------------")
        
    
          
     
def setZammad(pName, pLastname, pId, pEmail, pTel, pCompId) ->int:
    if(searchZammadByBitrixId(pId)==0):
        l=listCompanies()
        x = 0
        n = 'keine Angabe'
        for item in l:
            
            if(pCompId in l[x][1]  ):
                n = l[x][0][1]
                           
            x = x+1     
        data = {
            "bitrixuid": pId,
            "firstname": pName,
            "lastname": pLastname,
            "email": pEmail,
            "phone": pTel,
            "organization": n, 
                }
        r = requests.post(ZAMMAD_URL, auth=('jan.hegemann@dnn-marketing.de', 'Janni2002'), json=data)
                
              
        return r
    else:
        return 0
        
def updateZammad(pName, pLastname, pId, pEmail, pTel, pCompId) ->int:
    l=listCompanies()
    x = 0
    n = 'keine Angabe'
    for item in l:
        
        if(pCompId in l[x][1]):
            n = l[x][0][1]
            
        
        x = x+1   
    data = {
            
            "firstname": pName,
            "lastname": pLastname,
            "email": pEmail,
            "phone": pTel,
            "organization": n, 
                }
    c = str(searchZammadByBitrixId(pId))        
    r = requests.put(ZAMMAD_URL+c, auth=('jan.hegemann@dnn-marketing.de', 'Janni2002'), json=data)
    
    return r

def searchZammadByBitrixId(pId) :
    j = requests.get(ZAMMAD_URL, auth=('jan.hegemann@dnn-marketing.de', 'Janni2002')).json()
    b = 0
    for item in j:
        if(j[b]['bitrixuid'] != None):
            if (pId in j[b]['bitrixuid'] ):
                a = j[b]
                r = a['id']        
            else:
                r =  0
        else:
            r = 0
        b = b+1
    return r
    
def setZammadC(pTitle, pId) ->int:
    if(searchZammadByBitrixIdC(pId)==0):
        data = {
            "bitrixooid": pId,
            "name": pTitle,
            
                }
        r = requests.post(ZAMMAD_URLC, auth=('jan.hegemann@dnn-marketing.de', 'Janni2002'), json=data)
        
        return r
    else:
        return 0    
    
def updateZammadC(pTitle, pId) ->int:
    data = {
        
        "name": pTitle,
        
            }
    c = str(searchZammadByBitrixIdC(pId))        
    r = requests.put(ZAMMAD_URLC+c, auth=('jan.hegemann@dnn-marketing.de', 'Janni2002'), json=data)
    return r
    
    
def searchZammadByBitrixIdC(pId) :
    j = requests.get(ZAMMAD_URLC, auth=('jan.hegemann@dnn-marketing.de', 'Janni2002')).json()
    b = 0
    for item in j:
        if(j[b]['bitrixooid'] != None):
            if (pId in j[b]['bitrixooid'] ):
                a = j[b]
                r = a['id']        
            else:
                r =  0
        else:
            r = 0        
        b = b+1
    return r     
    
def listCompanies():
    out = get_companies(BITRIX_URL, BITRIX_SECRET)
    l = [ ]
    x = -1
    for item in out:
        x = x+1 
        ll = []
        for key, value in out[x].items():
            if (isinstance(value, list)):
                ll.append((key, value[0]["VALUE"]))
            else:
                ll.append((key, value))
        l.append(ll)    
        id = (out[x]['ID'])
        title = (out[x]['TITLE'])
        
    return l       

 
try:
   syncBITRIXToZAMMAD()
except:
   loggen("An unknown Error occurred")    
        

	





	


